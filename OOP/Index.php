<?php

require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$binatang = new Animal("Shaun");

echo "Name = " . $binatang->type;
echo "<br>";
echo "Legs = " . $binatang->Legs;
echo "<br>";
echo "cold_blooded = " . $binatang->cold_blooded;
echo "<br>";

echo "<br>";

// $binatang = new Animal("Buduk");
$kodok = new Frog("Buduk");

echo "Name : " . $kodok->type;
echo "<br>";
echo "Legs : " . $kodok->Legs;
echo "<br>";
echo "cold_blooded : " . $kodok->cold_blooded;
echo "<br>";
echo $kodok->Jump();
echo "<br>";

echo "<br>";

$sungokong = new Ape("Kera Sakti");

echo "Name : " . $sungokong->type;
echo "<br>";
echo "Legs : " . $sungokong->legs;
echo "<br>";
echo "cold_blooded : " . $sungokong->cold_blooded;
echo "<br>";
echo $sungokong->yell();
echo "<br>";

?>